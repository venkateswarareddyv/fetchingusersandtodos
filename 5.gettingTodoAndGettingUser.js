function gettingMatchedUser(){
    fetch('https://jsonplaceholder.typicode.com/todos?id=1').then((response)=>{
        return response.json();
    }).then((firstTodo)=>{
        return firstTodo[0].userId
    }).then((userId)=>{
        return fetch(`https://jsonplaceholder.typicode.com/users?id=${userId}`)
    }).then((response)=>{
        return response.json();
    }).then((userMatched)=>{
        console.log(userMatched)
    })
    .catch((error)=>{
        console.log(error)
    })

}

gettingMatchedUser();

